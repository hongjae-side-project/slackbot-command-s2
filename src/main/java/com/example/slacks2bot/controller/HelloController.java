package com.example.slacks2bot.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @PostMapping("/hello")
    public String hello() {
        String resultJSON = "{\"response_type\":\"in_channel\",\"text\":\"test\"}";
        return resultJSON;
    }
}
