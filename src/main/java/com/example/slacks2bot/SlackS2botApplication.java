package com.example.slacks2bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlackS2botApplication {

    public static void main(String[] args) {
        SpringApplication.run(SlackS2botApplication.class, args);
    }

}
